import { If, UIMessageHelper } from 'rainbowui-desktop-core';
// import logoLeft from '../../image/left_2.png';
// import logoRight from '../../image/right_2.png';
// import logoMiddle from '../../image/img_Admin.jpg';
// import logo from '../../image/Admin_logo.png';
import { UrlUtil, Util } from 'rainbow-desktop-tools';
import { SessionContext } from 'rainbow-desktop-cache';
import r18n from '../../i18n/reactjs-tag.i18n.jsx';
import "../../css/loginPro.css";

export default class index extends React.Component {
    constructor(props) {
        super(props);
        this.user = {
            username: null,
            password: null
        };
        this.state = {
            isShowLoading: false,
            isLanguageDiff: false,
            shakeClass: 'hideShake'
        };
        const config = SessionContext.get('project_config') || {};
        let telentConfig = config.telentConfig;
        let skinConfig = {};
        if (telentConfig && telentConfig.mltdatetimepicker && typeof telentConfig.mltdatetimepicker === "string"){
            skinConfig = JSON.parse(telentConfig.mltdatetimepicker);
        }else if (telentConfig && telentConfig.mltdatetimepicker && typeof telentConfig.mltdatetimepicker === "object"){
           skinConfig = telentConfig.mltdatetimepicker;
        }
        this.isForgotPassword = config && config.telentConfig && config.telentConfig.hideForgotPassword? Util.parseBool(config.telentConfig.hideForgotPassword) : false;
    }
    componentDidMount() {
        sessionStorage.removeItem('Authorization');
        $('#page-loading').hide();
        document.addEventListener("keydown", this.handleEnterKey);
        let config = sessionStorage.getItem("project_config") ? JSON.parse(sessionStorage.getItem("project_config")) : null;
        if (config && config.telentConfig && config.telentConfig.AdminTitle) {
            $('#maintitle').html(config.telentConfig.AdminTitle)
        }
        if ($('#mainFavicon') && config && config.telentConfig && config.telentConfig.AdminFavicon) {
            $('#mainFavicon').attr({href:`${config.UI_API_GATEWAY_PROXY}static/logo/${config.telentConfig.AdminFavicon}`});
        }
    }
    render() {
        let skin = 'default';
        let logo = require(`../../image/Admin_logo_${skin}.png`);
        let logoLeft = require(`../../image/left_${skin}.png`);
        let logoRight = require(`../../image/right_${skin}.png`);
        let logoMiddle = require(`../../image/img_Admin_${skin}.jpg`);
        skin = this.props.skin ? this.props.skin : 'default';        
        try{
            logo = require(`../../image/Admin_logo_${skin}.png`);
        }catch(err){}
        try{
            logoLeft = require(`../../image/left_${skin}.png`);
            logoRight = require(`../../image/right_${skin}.png`);
            logoMiddle = require(`../../image/img_Admin_${skin}.jpg`);
        }catch(err){
            skin = 'default';
        }

        return (
            <div className="login_body">
                <div className="login_content">
                    <img className="login_content_left" src={logoLeft} width="387px" height="222px" />
                    <img className="login_content_right" src={logoRight} width="279px" height="160px" />
                    <div className="login_content_middle">
                        <img src={logoMiddle} />
                    </div>
                    <div className="login_content_card">
                        <img src={logo}  />
                        <div className="login_content_input">
                            <input placeholder={r18n.UserName} value={this.user.username} onChange={this.usernameChange.bind(this)} />
                            <input type="password" placeholder={r18n.Password} value={this.user.password} onChange={this.passwordChange.bind(this)} />
                            <div id="login_content_button" className={"login_content_button" + skin} onClick={this.login.bind(this)}>
                                <div>{r18n.Login}</div>
                            </div>
                        </div>
                        {/* <div className={this.state.shakeClass}>
                            <span>Incorrect username or password.</span>
                        </div> */}
                        <If condition={!this.isForgotPassword}>
                            <div className="forgot_password" onClick={this.forgotPassword.bind(this)}>{r18n.ForgotPassword}</div>
                        </If>
                    </div>
                </div>
                <div className="login_bottom">Copyright © {new Date().getFullYear()} eBaoTech Corporation. All rights reserved.</div>
            </div>
        );

    }
    async login() {
        let login = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'LOGIN');
        const config = SessionContext.get('project_config') || {};
        if(config.telentConfig&&config.telentConfig.LOGIN_URL){
            login = config.UI_API_GATEWAY_PROXY+config.telentConfig.LOGIN_URL
        }

        const setting = {
            'method': 'POST'
        };
        if (!this.user.password || !this.user.username) {
            UIMessageHelper.info('Username and password cannot be empty');
        } else {
            const data = await AjaxUtil.call(login, this.user, setting);
            if (data.authResult) {
                sessionStorage.setItem(config.key, 'access_token=' + data.access_token);
                sessionStorage.setItem('goInAdmin', Util.parseBool(this.props.goInAdmin));
                // let getUserInfoUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'USER_INFO');
                let getUserInfoUrl = config.UI_API_GATEWAY_PROXY + 'urp/public/users/v1/current/info';
                this.userInfoList = await AjaxUtil.call(getUserInfoUrl, null, { 'method': 'GET' });
                sessionStorage.setItem('UserInfo', JSON.stringify(this.userInfoList));            
                let getUserImageUrl = config.UI_API_GATEWAY_PROXY + 'urp/public/users/v1/getCurrentUserImage';                
                let userImage = await AjaxUtil.call(getUserImageUrl, null, { 'method': 'GET' });
                // this.userInfoList.Image = userImage;
                sessionStorage.setItem('UserImage', userImage);           
                this.getUserInfo();
                this.getUserLang();
                if (this.state.isLanguageDiff) {
                    // let tenant = sessionStorage.getItem('x-ebao-response-tenant-code');
                    // let homeObj = config['UI_LOGIN_HOME_' + tenant];
                    // if (homeObj) {
                    //     sessionStorage.setItem('skin', homeObj.skin);
                    //     sessionStorage.setItem('HOME_URL', homeObj.HOME_URL);
                    //     sessionStorage.setItem('homepage', homeObj.homepage);
                    //     sessionStorage.setItem('toplogo', homeObj.toplogo);
                    //     if (document.getElementById('skincolor')) {
                    //         if (homeObj.skin == 'blue') {
                    //             document.getElementById('skincolor').href = config.UI_API_GATEWAY_PROXY + '/static/rainbow/ui/3.0.0/blue-core.css';
                    //         } else if (homeObj.skin == "default") {
                    //             document.getElementById('skincolor').href = config.UI_API_GATEWAY_PROXY + '/static/rainbow/ui/3.0.0/core.css'
                    //         } else if (!homeObj.skin) {
                    //             document.getElementById('skincolor').href = config.UI_API_GATEWAY_PROXY + '/static/rainbow/ui/3.0.0/core.css'
                    //         }
                    //     }
                    // }
                    if(sessionStorage.getItem('BeforePath')){
                        let BeforePath = sessionStorage.getItem('BeforePath');
                        sessionStorage.removeItem('BeforePath')
                        window.location.hash = BeforePath;                   
                        // window.location.reload();
                    }else{
                        let hrefUrl = window.location.origin+window.location.pathname;
                        window.location.href = hrefUrl;
                        // window.location.hash = 'home';
                    }
                }
            } else {
                UIMessageHelper.info('Incorrect username or password');
            }
        }

    }


    getUserInfo() {
        const getUserUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'USER_INFO');

        $.ajax({
            method: 'GET',
            url: getUserUrl,
            async: false,
            contentType: 'application/json;charset=UTF-8',
            xhrFields: { withCredentials: true },
            crossDomain: true,
            beforeSend: function (xhr) {
                let authorization = SessionContext.get('Authorization');
                if (authorization == null || authorization == undefined || authorization == '') {

                } else {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
                }
            },
            success: function (d, status, xhr) {
                SessionContext.put('UserInfo', d)
                // SessionContext.put('USER', d);
            },
            error: function (error) {
                if (error.status == 403 || error.status == 401) {
                    const cfg = SessionContext.get('project_config');
                    logout(cfg);
                } else {
                    // window.toastr?toastr['error']('I18n API Error.', 'ERROR'):null;
                }
            }
        });
    }

    getUserLang() {
        let langUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'GET_USER_LANG');
        let _self = this
        $.ajax({
            method: 'GET',
            url: langUrl,
            async: false,
            contentType: 'application/json;charset=UTF-8',
            xhrFields: { withCredentials: true },
            crossDomain: true,
            beforeSend: function (xhr) {
                let authorization = SessionContext.get('Authorization');
                if (authorization == null || authorization == undefined || authorization == '') {
                } else {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
                }
            },
            success: function (d, status, xhr) {
                if (d) {
                    // localStorage.setItem('system_i18nKey', d);
                    sessionStorage.setItem('system_i18nKey', d);
                }
                const env = sessionStorage.getItem('x-ebao-env');
                const tenant = sessionStorage.getItem('x-ebao-response-tenant-code');
                let henv = xhr.getResponseHeader('x-ebao-env');
                let htenant = xhr.getResponseHeader('x-ebao-response-tenant-code');
                if (!env && henv) {
                    sessionStorage.setItem('x-ebao-env', henv);
                }
                if (!tenant && htenant) {
                    sessionStorage.setItem('x-ebao-response-tenant-code', htenant);
                }
                let lang = localStorage.getItem('default_system_i18nKey')
                let projectPathKey = window.buildKey(window.currentUrl);
                let i18n_cache_key = 'i18n_Cache_' + lang + '_' + projectPathKey;
                if (d !== lang) {
                    _self.setState({ isLanguageDiff: true })
                }
            },
            error: function (error) {
                if (error.status == 403 || error.status == 401) {
                    const cfg = SessionContext.get('project_config');
                    logout(cfg);
                } else {
                    // window.toastr?toastr['error']('I18n API Error.', 'ERROR'):null;
                }
            }
        });
    }

    usernameChange(e) {
        this.user.username = e.target.value
    }

    passwordChange(e) {
        this.user.password = e.target.value
    }

    handleEnterKey(e) {
        var theEvent = e || window.event;
        var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
        if (code == 13) {
            document.getElementById("login_content_button").click()
        }
    }

    async forgotPassword(){
        const config = SessionContext.get('project_config');
        let forgotPasswordUrl = config.UI_API_GATEWAY_PROXY + 'v1/forgotPassword';
        let forgotWindowUrl = await AjaxUtil.call(forgotPasswordUrl, null, { 'method': 'GET' });
        window.open(forgotWindowUrl);
    }
}