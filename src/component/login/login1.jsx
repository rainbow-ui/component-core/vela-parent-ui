import { UICell, UICard, UIMessageHelper, UIText, UIButton, UIPassword } from 'rainbowui-desktop-core';
import logo from '../../image/logo_mo.png';
import { UrlUtil, Util } from 'rainbow-desktop-tools';
import { SessionContext } from 'rainbow-desktop-cache';
import "../../css/login.css";

export default class index extends React.Component {
    constructor(props) {
        super(props);
        this.user = {
            username: null,
            password: null
        };
        this.state = {
            isShowLoading: false,
            isLanguageDiff: false,
            shakeClass: 'hideShake'
        };
    }
    componentDidMount() {
        sessionStorage.removeItem('Authorization');
        $('#page-loading').hide();
    }
    render() {
        return (
            <div>
                <div className="home-blank"></div>
                <div class='loginbg'>
                    <div class="loginbg-filter">
                        <div class="login-con">
                            <div class="login-des">
                                <h1>Insurance Middle Office</h1>
                                <span>a Middleware of Insurance Industry</span>
                            </div>
                            <div class="login-form">
                                <UICard width="12" outline='default' collapseIcon="false">
                                    <UICell width="12" id="component">
                                        <UICell width="12">
                                            <img src={logo} width="100px" style={{ marginTop: '10px' }} />
                                        </UICell>
                                        <UICell width="12">
                                            <UIText noI18n='true' prefixIcon="rainbow User" placeHolder="User Name" model={this.user} property="username" />
                                            {/* <UIText noI18n='true' placeHolder="用户名" model={this.user} property="username" /> */}
                                        </UICell>
                                        <UICell width="12" className="password" >
                                            <UIPassword noI18n='true' prefixIcon="rainbow Lock" suffixIcon="rainbow HelpCircle" placeHolder="Password" model={this.user} property="password" />
                                        </UICell>
                                        <UICell width="12">
                                            <UIButton noI18n='true' value="Sign in" styleClass="primary" isLoading={this.state.isShowLoading} style={{ minWidth: '208px', marginBottom: '30px', display: 'flex', justifyContent: 'center' }} onClick={this.login.bind(this)} />
                                        </UICell>
                                        <UICell width="12">
                                            <div class={this.state.shakeClass}>
                                                <span>Incorrect username or password.</span>
                                            </div>
                                        </UICell>
                                    </UICell>
                                </UICard>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="home-blank"></div>
            </div>
        );

    }
    async login() {
        const login = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'LOGIN');
        const auth = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'AUTH');
        const config = SessionContext.get('project_config');
        const setting = {
            'method': 'POST'
        };
        if (!this.user.password || !this.user.username) {
            UIMessageHelper.info('用户名密码不能为空');
        } else {
            this.setState({ isShowLoading: true });
            const user = await AjaxUtil.call(auth, this.user, setting);
            if (user.authResult) {
                const data = await AjaxUtil.call(login, this.user, setting);
                sessionStorage.setItem(config.key, 'access_token=' + data.access_token);
                sessionStorage.setItem('goInAdmin', Util.parseBool(this.props.goInAdmin));
                let getUserInfoUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'USER_INFO');
                this.userInfoList = await AjaxUtil.call(getUserInfoUrl, null, { 'method': 'GET' });
                sessionStorage.setItem('UserInfo', JSON.stringify(this.userInfoList));
                this.getUserInfo();
                this.getUserLang();
                window.location.hash = 'home';
                this.setState({ isShowLoading: false, shakeClass: 'hideShake' });
                if (this.state.isLanguageDiff){
                    location.reload()
                }
            } else {
                // UIMessageHelper.info('用户名密码不正确');
                this.setState({ isShowLoading: false, shakeClass: 'showShake shake_horizontal' });
                setTimeout(() => {
                    this.setState({ shakeClass: 'showShake' });
                }, 1000);
            }
        }

    }


    getUserInfo() {
        const getUserUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'USER_INFO');

        $.ajax({
            method: 'GET',
            url: getUserUrl,
            async: false,
            contentType: 'application/json;charset=UTF-8',
            xhrFields: { withCredentials: true },
            crossDomain: true,
            beforeSend: function (xhr) {
                let authorization = SessionContext.get('Authorization');
                if (authorization == null || authorization == undefined || authorization == '') {

                } else {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
                }
            },
            success: function (d, status, xhr) {
                SessionContext.put('UserInfo', d)
                // SessionContext.put('USER', d);
            },
            error: function (error) {
                if (error.status == 403 || error.status == 401) {
                    const cfg = SessionContext.get('project_config');
                    logout(cfg);
                } else {
                    // window.toastr?toastr['error']('I18n API Error.', 'ERROR'):null;
                }
            }
        });
    }

    getUserLang() {
        let langUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'GET_USER_LANG');
        let _self = this
        $.ajax({
            method: 'GET',
            url: langUrl,
            async: false,
            contentType: 'application/json;charset=UTF-8',
            xhrFields: { withCredentials: true },
            crossDomain: true,
            beforeSend: function (xhr) {
                let authorization = SessionContext.get('Authorization');
                if (authorization == null || authorization == undefined || authorization == '') {
                } else {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
                }
            },
            success: function (d, status, xhr) {
                if (d) {
                    localStorage.setItem('system_i18nKey', d);
                }
                const env = sessionStorage.getItem('x-ebao-env');
                const tenant = sessionStorage.getItem('x-ebao-response-tenant-code');
                let henv = xhr.getResponseHeader('x-ebao-env');
                let htenant = xhr.getResponseHeader('x-ebao-response-tenant-code');
                if (!env && henv) {
                    sessionStorage.setItem('x-ebao-env', henv);
                }
                if (!tenant && htenant) {
                    sessionStorage.setItem('x-ebao-response-tenant-code', htenant);
                }
                let lang = localStorage.getItem('default_system_i18nKey')
                let projectPathKey = window.buildKey(window.currentUrl);
                let i18n_cache_key = 'i18n_Cache_' + lang + '_' + projectPathKey;
                if (d !== lang){
                    _self.setState({isLanguageDiff: true})
                }
            },
            error: function (error) {
                if (error.status == 403 || error.status == 401) {
                    const cfg = SessionContext.get('project_config');
                    logout(cfg);
                } else {
                    // window.toastr?toastr['error']('I18n API Error.', 'ERROR'):null;
                }
            }
        });
    }

}

