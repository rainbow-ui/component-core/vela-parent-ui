import "../../css/tabContainer.css";
import PropTypes from 'prop-types';
import { Util } from 'rainbow-desktop-tools';

export default class TabContainer extends React.Component {
    constructor(props) {
        super(props);
       
    }
    render () {
      const goInAdmin = sessionStorage.getItem('goInAdmin')
        return (
            <div class={ Util.parseBool(goInAdmin) ?'admin-home-page-outside-tab':'home-page-outside-tab'}>
                <div class='home-page-outside-tab-warp'>
                    <div class='home-page-item-tab-wrap'>
                        {this.props.title}
                    </div>
                </div>
                <div class="home-page-item-tab-wrap-tabcontainer"></div>
                <div className={"home-page-item-tab-wrap-activeIndex"}>
                {this.props.children} 
                </div>
            </div>
        );
    }
}

/**@ignore
 * Tab component prop types
 */
TabContainer.propTypes = $.extend({}, {
  title: PropTypes.string
});

/**@ignore
* Get tab component default props
*/
TabContainer.defaultProps = $.extend({}, {
});
