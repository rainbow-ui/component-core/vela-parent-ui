import summaryImg from "../../image/summary-img.png";
import { Util } from 'rainbow-desktop-tools';

export default class index extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title: props.title,
      content: props.content,
      showImg: props.showImg ? props.showImg : false,
      fixed: props.fixed ? props.fixed : false,
      contentJson: props.contentJson,
      tabJson: props.tabJson,
      isInner: props.isInner ? props.isInner : false,
      height: 100
    }
  }
  componentDidMount() {
    this.state.height = document.getElementById("top-card").offsetHeight;
    this.setState({ height: this.state.height })
  }
  componentWillUpdate(nextProps, nextState) {
    this.state.height = document.getElementById("top-card").offsetHeight;
    this.state.title = nextProps.title,
      this.state.content = nextProps.content,
      this.state.showImg = nextProps.showImg ? nextProps.showImg : false,
      this.state.fixed = nextProps.fixed ? nextProps.fixed : false,
      this.state.contentJson = nextProps.contentJson,
      this.state.isInner = nextProps.isInner ? nextProps.isInner : false,
      this.state.tabJson = nextProps.tabJson
  }

  componentWillReceiveProps() {
  }
  render() {
    const contentList = []
    let contentJson = this.state.contentJson
    let tabJson = this.state.tabJson
    let canShow = false
    if (!_.isEmpty(contentJson)) {
      for (let key in contentJson) {
        if(!_.isString(contentJson[key])&&_.isObject(contentJson[key])){
          canShow = true
          contentList.push(
            <div class="home-page-item-wrap-item">
              <div class="home-page-item-title">{key}</div>
              <div class="home-page-item-value top-link" onClick={contentJson[key].onClick}>{contentJson[key].value}</div>
            </div>
          )
        }else{
          canShow = true
          contentList.push(
            <div class="home-page-item-wrap-item">
              <div class="home-page-item-title">{key}</div>
              <div class="home-page-item-value">{contentJson[key]}</div>
            </div>
          )
        }
      }
    }
    if (!_.isEmpty(tabJson)) {
      _.each(tabJson, (item) => {
        contentList.push(
          <div class="home-page-item-wrap-item" onClick={this.onClick.bind(this, item)} style={{
            "cursor": "pointer"
          }}>
            <div class={Util.parseBool(item.isActive) ? "home-page-item-title-active" : "home-page-item-title"}>{i18n[item.value]}</div>
            <div class={Util.parseBool(item.isActive) ? "home-page-item-value-active" : "home-page-item-value"}
              style={{
                "display": "flex", "justify-content": "center",
                "visibility": Util.parseBool(item.isActive) ? 'visible' : 'hidden'
              }}>
              <span class="rainbow Search"></span></div>
          </div>
        )
      })
    }
    let isLocal = !Util.parseBool(sessionStorage.getItem("goInAdmin")?sessionStorage.getItem("goInAdmin"): false)
    return (
      <div style={{ display: canShow || this.state.content || this.props.children ? 'block' : 'none', background: '#fff' }}>
        <div id="top-card" class="top-card" style={{position: this.state.fixed ? "fixed" : "unset", zIndex: "100", width: this.state.fixed?(isLocal?"calc(100% - 258px)":"calc(100% - 8px)"):"100%",right: this.state.fixed?"8px": "0"}}>
          {this.renderTitle()}
          <div class='top-card-body'>
            <div style={{ width: '100%' }}>
              {
                this.state.content ?
                  <div class='top-card-content'>{this.state.content}</div>
                  :
                  <noscript />
              }
              {
                !_.isEmpty(this.state.contentJson) ?
                  <div class="home-page-item-wrap">
                    {contentList}
                    {Util.parseBool(this.state.isInner) ? this.props.children : null}
                  </div>
                  :
                  <noscript />
              }
              {Util.parseBool(this.state.isInner) ? null : this.props.children}
            </div>
            {/* {this.renderRightImg()} */}
          </div>
        </div>
        {this.renderBack()}
      </div>
    )
  }

  renderTitle() {
    if (this.state.title) {
      return (<div class='top-card-title'>{this.props.noI18n?this.state.title:i18n[this.state.title] ? i18n[this.state.title] : 'i18n.' + this.state.title}</div>)
    } else {
      return (<div style={{ display: 'none' }} />)
    }
  }

  renderRightImg() {
    if (Util.parseBool(this.state.showImg)) {
      return (<img src={summaryImg} class="top-card-img" />)
    } else {
      return (<div style={{ display: 'none' }} />)
    }
  }

  renderBack() {
    return (
      this.state.fixed ?
        <div style={{ height: this.state.height }}></div> :
        <div></div>
    )
  }
  onClick(item) {
    if (this.props.onClick) {
      this.props.onClick(item);
    }
  }
}
