import { Component } from "rainbowui-desktop-core";
import { LocalContext } from 'rainbow-desktop-cache';
import PropTypes from 'prop-types';
import "../../css/commonconfig.css";
import frameImg from "../../image/frame.png";
import CarmiddlePng from "../../image/Car_middle.png";
import CartopPng from "../../image/Car_top.png";
import CloudmiddlePng from "../../image/Cloud_middle.png";
import CloudtopPng from "../../image/Cloud_top.png";
import ComputermiddlePng from "../../image/Computer_middle.png";
import ComputertopPng from "../../image/Computer_top.png";
import MobilemiddlePng from "../../image/Mobile_middle.png";
import MobiletopPng from "../../image/Mobile_top.png";
import WifimiddlePng from "../../image/Wifi_middle.png";
import WifitopPng from "../../image/Wifi_top.png";
import config from 'config';

export default class CommonConfigBackGround extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // var changebb = function() {
        //     $('#Wifimiddle').animate({opacity: 1}, 600).animate({opacity:0}, 600, function() {
        //         $('#Mobilemiddle, #Carmiddle').delay(1200).animate({opacity: 1}, 600).animate({opacity:0}, 600, function() {
        //             $('#Wifimiddle').delay(2400).animate({opacity: 1 }, 600).animate({opacity: 0 }, 600, function () {
        //                 $('#Cloudmiddle').delay(3600).animate({opacity: 1}, 600).animate({opacity: 0}, 600, function() {
        //                     $('#Computermiddle').delay(4800).animate({ opacity: 1 }, 600).animate({opacity: 0}, 600, changebb)
        //                 })
        //             })
        //         })
        //     })
        // }

        // changebb();

        var change = function(){
            $('#Wifimiddle').animate({
                opacity: 1
            }, 600).animate({
                opacity: 0
            }, 600, change).delay(4800)
        }


        var mobileAni = function(){
            $('#Mobilemiddle, #Carmiddle').delay(1300).animate({
                opacity: 1
            }, 600).animate({
                opacity: 0
            }, 600, mobileAni).delay(3600)
        }


        var change2 = function(){
            $('#Wifimiddle').delay(2400).animate({
                opacity: 1
            }, 600).animate({
                opacity: 0
            }, 600, change2).delay(2400)
        }

        var cloudAni = function() {
            $('#Cloudmiddle').delay(3600).animate({
                opacity: 1
            }, 600).animate({
                opacity: 0
            }, 600, cloudAni).delay(1200)
        }
  
        var computeAni = function() {
            $('#Computermiddle').delay(4800).animate({
                opacity: 1
            }, 600).animate({
                opacity: 0
            }, 600, computeAni).delay(600)
        }

         change();
         mobileAni();
         change2();
         cloudAni();
         computeAni();
        
    }

    componentDidUpdate() {
        $('#Wifimiddle').animate({
            opacity: '0'
        }, {
            duration: '2000'
        })
    }

    renderComponent() {
        const localLang = LocalContext.get(config.DEFAULT_LOCALSTORAGE_I18NKEY);
        if (localLang != null || localLang != undefined || localLang != "") {
            lang = localLang
        } else {
            lang = config.DEFAULT_SYSTEM_I18N
        }
        return (
            <div className="commonIndex">
                <div className="imgContainer">
                    <div className="productHomeDataTitle">
                        <div>{this.props.mainTitle}</div>
                        {lang == 'zh_CN' ?
                        <div>{this.props.subTitle}</div>
                        : ''
                        }
                    </div>
                    <div className="ditu">
                        <img id="frameImg" src={frameImg}/>
                        <div className="circleitem">
                            <div className="Wifimiddle" id="Wifimiddle">
                                <img src={WifimiddlePng}/>
                            </div>
                            <div className="wifitop">
                                <img id="WifitopPng" src={WifitopPng}/>
                            </div>
                        </div>

                        <div className="circleitem2">
                            <div className="Mobilemiddle" id="Mobilemiddle">
                                <img src={MobilemiddlePng}/>
                            </div>
                            <div className="Mobiletop">
                                <img id="Mobiletop" src={MobiletopPng}/>
                            </div>
                        </div>

                        <div className="circleitem3">
                            <div className="Carmiddle" id="Carmiddle">
                                <img  src={CarmiddlePng}/>
                            </div>
                            <div className="Cartop">
                                <img id="Cartop" src={CartopPng}/>
                            </div>
                        </div>

                        <div className="circleitem4">
                            <div className="Computermiddle" id="Computermiddle">
                                <img  src={ComputermiddlePng}/>
                            </div>
                            <div className="Computertop">
                                <img id="Computertop" src={ComputertopPng}/>
                            </div>
                        </div>

                        <div className="circleitem5">
                            <div className="Cloudmiddle" id="Cloudmiddle">
                                <img  src={CloudmiddlePng}/>
                            </div>
                            <div className="Cloudtop">
                                <img id="Cloudtop" src={CloudtopPng}/>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="homeIndexCopyRight">
                        <div>版权所有 &copy; 中国大地财产保险股份有限公司</div>
                </div>
            </div>
        )
    }
}

CommonConfigBackGround.propTypes = $.extend({}, Component.PropTypes, {
    mainTitle: PropTypes.string,
    subTitle: PropTypes.string
});

CommonConfigBackGround.defaultProps = $.extend({}, Component.defaultProps, {
    mainTitle: '',
    subTitle: ''
})