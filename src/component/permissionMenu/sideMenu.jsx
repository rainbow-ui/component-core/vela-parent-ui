import { UIMenuBar, UIMenu, UISubMenu, UIMenuItem, UISideNav, I18nUtil, UICell, UIBreadcrumb, UIBreadcrumbItem, UIDrawer, UIMessageHelper, UIDialog, UIBox, UIButton, UISmartPanelGrid, UIPassword, UIChooseList } from 'rainbowui-desktop-core';
import { UrlUtil, Util } from 'rainbow-desktop-tools';
import { LocalContext, PageContext, StoreContext } from 'rainbow-desktop-cache';
import r18n from '../../i18n/reactjs-tag.i18n.jsx';
import '../../css/sideMenu.css';

export default class index extends React.Component {
    constructor(props) {
        super(props);
        this.config = JSON.parse(sessionStorage.getItem('project_config')) || {};
        const skinStyle = localStorage.getItem('skin_Style_ignore') || null;
        this.state = {
            searchDataList: [],
            condition: {},
            breadCrumbs: [],
            postSettings: { block: null, method: 'POST', dataType: 'json', contentType: 'application/json; charset=UTF-8' },
            data: [],
            pwd: {},
            isShowRightSide: false,
            skin: skinStyle ? skinStyle : this.config && this.config.telentConfig && this.config.telentConfig.skin ? this.config.telentConfig.skin : 'default'
        };
    }

    onChangeUrl(customerUrl) {
        let url = UrlUtil.getConfigUrl('Center') + 'ui/' + customerUrl + '/#/';
        window.open(url);
    }
    openDialog() {
        UIDialog.show("edit-pwd");
    }

    changeSkin(skinInfo) {
        let skin = skinInfo.Skin;
        let skincolor = document.getElementById('skincolor');
        if (skincolor) {
            if (skin && skin != 'default') {
                skincolor.href = `${this.config.UI_API_GATEWAY_PROXY}static/rainbow/ui/3.0.0/${skin}-core.min.css?v=${this.random}`;
            } else {
                skincolor.href = `${this.config.UI_API_GATEWAY_PROXY}static/rainbow/ui/3.0.0/core.min.css?v=${this.random}`;
            }
            localStorage.setItem('skin_Style_ignore', skin);
            this.setState({skin: skin})
        }
        this.setState({isShowRightSide: false});

    }

    render() {
        let menuList = [];
        let telentConfig = this.config.telentConfig||{};
        let skinList = [];
        let allowLanguage = true;
        let showPersonalCenter = true;

        let skinConfig = null;
        if (telentConfig && telentConfig.UI_SKIN_STYLE && typeof telentConfig.UI_SKIN_STYLE === 'string') {
            skinConfig = JSON.parse(telentConfig.UI_SKIN_STYLE);
        } else if (telentConfig && telentConfig.UI_SKIN_STYLE && typeof telentConfig.UI_SKIN_STYLE === 'object') {
            skinConfig = telentConfig.UI_SKIN_STYLE;
        }
        if(telentConfig && telentConfig.UI_SKIN_GROUP && typeof telentConfig.UI_SKIN_GROUP === 'string'){
            let skinList = telentConfig.UI_SKIN_GROUP.split(',') || [];
            if( skinList && skinList.length > 0){
                skinConfig = []
                skinList.forEach(item => {
                    let skinItem = {};
                    skinItem.Skin = item;
                    skinConfig.push(skinItem);
                });
            }
            
        }

        

        if (!skinConfig) {
            skinConfig = skinConfig ? skinConfig : [
                {
                    "Skin": "blue",
                    "URL": this.config.UI_API_GATEWAY_PROXY + "static/rainbow/ui/3.0.0/blue-core.css"
                },
                {
                    'Skin': 'red',
                    'URL': this.config.UI_API_GATEWAY_PROXY + 'static/rainbow/ui/3.0.0/red-core.css'
                },
                {
                    "Skin": "default",
                    "URL": this.config.UI_API_GATEWAY_PROXY + "static/rainbow/ui/3.0.0/core.css"
                }
            ];

        } else {
            allowLanguage = true
        }
        allowLanguage = telentConfig.allowLanguage != undefined ? Util.parseBool(telentConfig.allowLanguage) : true;
        showPersonalCenter = telentConfig.showPersonalCenter != undefined ? Util.parseBool(telentConfig.showPersonalCenter) : true;


        if (this.menuList && this.menuList.length > 0 && allowLanguage) {
            menuList = this.menuList.map( item => {
                return { itemText: item.LanguageId, obj: item }
            });
        }

        if (skinConfig && skinConfig.length > 0) {
            skinList = skinConfig.map(item => {
                return {itemText: item.Skin, obj: item};
            })
        }


        // const menu = $('#cloud_sideMenu')
        const goInAdmin = sessionStorage.getItem('goInAdmin')
        if (window.location.href.indexOf('isShowTopMenu=false') > -1 || Util.parseBool(goInAdmin)) {
            return (
                <div>
                    <div style={{ display: 'flex' }}>
                        <div id="desktop-right-page" class="flex-width-full">{this.props.children}</div>
                    </div>
                </div>)
        } else {
            return (
                <div>
                    <div style={{ display: 'flex',height: '100vh'}}>
                        <UISideNav id="cloud_sideMenu" logo={this.props.logo} logoMin={this.props.logoMin} data={this.state.data} openSideMenu="true" nodeClick={this.props.onClick} />
                        <div id="desktop-right-page" class="flex-width-full">
                            <UIMenuBar styleClass={this.props.rightLogoUrl ? "white menuNavMenuBar" : 'white menuNavMenuBar navbarNoline'} toplogo={this.props.rightLogoUrl ? this.props.rightLogoUrl : ''} subHeaderTitle={this.buildBreadCrumbs.bind(this)}>
                                <UICell styleClass="flex" class="menuNavMenuBar-cell">
                                    {/* <UICell type="flex" /> */}
                                    <UICell type="flex" className="navMenu">
                                        <span class="rainbow Duplicate menu_window" title="Open New Admin" onClick={this.openAdmin.bind(this)} />
                                        <span id="SrceenExtend" class="rainbow ScreenExtend menu_window" title="Srceen Extend" onClick={this.SrceenExtend.bind(this)} />
                                        <div style={{display: 'flex', alignItems: 'center', marginLeft: '15px'}}>
                                            <span id="SrceenContract" class="rainbow ScreenContract menu_window icon_display" title="Srceen Contract" onClick={this.SrceenContract.bind(this)} />
                                            <img src='https://rainbow.ebaotech.com/static/rainbow/image/favicon.ico' className="" style={{margin: '0', marginRight: '5px', borderRadius: '50%'}}/>
                                            <UIMenu style={{ justifyContent: 'flex-end' }} >
                                                <UISubMenu value={this.userInfoList && this.userInfoList.hasOwnProperty('UserName') ? this.userInfoList.UserName : ''} noI18n="true" className="dropdown-menu-right">
                                                    <UIMenuItem value="ChangePassword"  visibled={showPersonalCenter} onClick={this.openDialog.bind(this)} />
                                                    <UIMenuItem value="Logout" onClick={this.onClickLogout.bind(this)} />
                                                </UISubMenu>
                                            </UIMenu>
                                        </div>
                                        <div style={{marginLeft: '15px', fontSize: '20px'}}>
                                            <span class="rainbow Setting" onClick={() => this.setState({isShowRightSide: true})}></span>
                                        </div>
                                        
                                    </UICell>
                                </UICell>
                            </UIMenuBar>
                            <UIDrawer open={this.state.isShowRightSide} width="320px" onClose={() => {this.setState({isShowRightSide: false})}}>
                                <div class="contentBox">
                                    <UIChooseList title="Language" contentList={menuList} activeItem={sessionStorage.getItem('system_i18nKey')} onClickFunc={this.selectLanguage.bind(this)}/>
                                    <UIChooseList title="Mode" contentList={skinList} activeItem={this.state.skin} noi18n={true} onClickFunc={this.changeSkin.bind(this)}/>
                                </div>
                            </UIDrawer>
                            {this.props.children}
                            <UIDialog id="edit-pwd" width="500px">
                                <UISmartPanelGrid column="1">
                                    <UIPassword label="oldPassword" model={this.state.pwd} property="oldPassword" required="true" />
                                    <UIPassword label="newPassword" model={this.state.pwd} property="newPassword" required="true" />
                                    <UIPassword label="repeatPassword" model={this.state.pwd} property="repeatPassword" required="true" />
                                </UISmartPanelGrid>
                                <UIBox direction="right">
                                    <UIButton id="cancel-edit" styleClass="default" value="cancel" onClick={this.cancelEdit.bind(this)}></UIButton>
                                    <UIButton id="confirm-edit" styleClass="primary" value="confirm" onClick={this.confirmEditPwd.bind(this)}></UIButton>
                                </UIBox>
                            </UIDialog>
                        </div>
                    </div>  </div>
            );
        }
    }

    async componentDidMount() {
        if (LocalContext.get('UserLanguageList')) {
            this.menuList = LocalContext.get('UserLanguageList')
        } else {
            let menuListUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'GET_LANGUAGE_LIST');
            this.menuList = await AjaxUtil.call(menuListUrl, null, { 'method': 'GET' });
            LocalContext.put('UserLanguageList', this.menuList)
        }
        let getUserInfoUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'USER_INFO');
        this.userInfoList = await AjaxUtil.call(getUserInfoUrl, null, { 'method': 'GET' });
        sessionStorage.setItem('UserInfo', JSON.stringify(this.userInfoList));
        if (window && window.parent && window.parent.location && window.parent.location.pathname && window.parent.location.pathname.indexOf('ui/admin') < 0 ) {
            let menuListUrl = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'GET_MENU');
            let allMenuList = await AjaxUtil.call(menuListUrl, null, { 'method': 'GET' });
            //校验meun中是否存在
            if(window.location.host.indexOf("localhost") < 0){
                let homeList = [];
                // let hrefPart = ''
                // let herf = window.location.href;
                // if(herf && herf.indexOf('?') > -1){
                //     herf = herf.split('?')[0];
                // }
                // if(herf && herf.indexOf('/#/') > -1){
                //     hrefPart = herf.split('/#/') && herf.split('/#/')[1] ? '#/' + herf.split('/#/')[1] : '';
                // }
                // const homepageSplit = window.location.pathname + hrefPart;
                const homepageSplit = window.location.pathname;
                this.menuHasHome(allMenuList,homepageSplit,homeList);
                if(homeList.length == 0){
                    UIMessageHelper.warning(r18n.noAccess);
                    window.location.href = this.config.UI_API_GATEWAY_PROXY + 'ui/admin/#/';
                }else{
                    sessionStorage.setItem('goInAdmin', true);
                }
            }
            this.setState({ data: allMenuList });
        }
        this.forceUpdate();
    }

    menuHasHome(allMenuList,homepage,homeList){
        allMenuList.forEach(element => {
            if(element.EntranceUrl&&element.EntranceUrl.indexOf(homepage)>-1){
                homeList.push(element)
                return homeList;
            }else{
                if(element&&Util.parseBool(element.HasChild)&&element.Children&&element.Children.length>0){
                    this.menuHasHome(element.Children,homepage,homeList)
                }
            }
        });
    }

    findChild(obj, arr) {
        if (!obj.Children) {
            return;
        }
        for (let i = 0; i < obj.Children.length; i++) {
            for (let j = 0; j < arr.length; j++) {
                if (obj.Children[i].Id == arr[j]) {
                    this.state.breadCrumbs.push({ txt: obj.Children[i].Value, link: obj.Children[i].onClick });
                    this.findChild(obj.Children[i], arr);
                }
            }
        }
    }


    changeItem() {
        this.state.breadCrumbs = [];
        let menuData = PageContext.get('deepPath');
        if (menuData && menuData.length > 0) {
            let arr = menuData.split('-');

            for (let i = 0; i < this.state.data.length; i++) {
                let item = this.state.data[i];
                for (let j = 0; j < arr.length; j++) {
                    if (item.Id == arr[j]) {
                        this.state.breadCrumbs.push({ txt: item.Value, link: '' });
                        this.findChild(item, arr);
                    }
                }
            }
        }
    }

    buildBread() {
        let arr = [];
        for (let i = 0; i < this.state.breadCrumbs.length; i++) {
            arr.push(<UIBreadcrumbItem noI18n="true" title={this.state.breadCrumbs[i].txt} />);
        }
        return arr;
    }
    buildBreadCrumbs() {
        this.changeItem();
        return (
            <UICell type="flex" style={{ 'display': 'flex', 'width': '500px', 'justifyContent': 'flex-start', 'height': '25px' }}>
                {/* <span style={{'color': '#515a6e','cursor': 'pointer'}}>
                <span class="rainbow Home" style={{'line-height': '20px'}}></span>
                <span style={{'margin-left': '5px'}}>首页</span>
                <span style={{'margin-left': '5px','display': isShow}}>/</span>
                </span> */}
                <span id="menuSwitchOn" class="rainbow MenuFolded menu_switch" onClick={this.menuSwitchOn.bind(this)} />
                <span id="menuSwitchOff" class="rainbow MenuUnfolded menu_switch icon_display" onClick={this.menuSwitchOff.bind(this)} />
                <UICell type="flex">
                    <UIBreadcrumb >
                        {this.buildBread()}
                    </UIBreadcrumb>
                </UICell>
            </UICell>
        );
        // return "Home"
    }

    async selectLanguage(obj) {
        // localStorage.clear();
        // await StoreContext.clear();
        let languageId = obj && obj.LanguageId ? obj.LanguageId : 'en_US';
        AjaxUtil.show()
        let set_user_lang_url = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'SET_USER_LANG');
        await AjaxUtil.call(set_user_lang_url, { 'langId': languageId }, this.state.postSettings);
        I18nUtil.setSystemI18N(languageId);
        //只能清理部分缓存
        _.each(localStorage, (item, index) => {
            let key = localStorage.key(index); //获取本地存储的Key
            if (key) {
                if (key.indexOf('i18n_Cache') > -1) {
                    localStorage.removeItem(key)
                }
            }
        })
        _.each(sessionStorage, (item, index) => {
            let key = sessionStorage.key(index); //获取本地存储的Key
            if (key) {
                if (key.indexOf('LOAD_CODETABLE') > -1) {
                    sessionStorage.removeItem(key)
                }
            }
        })
        // indexedDB.deleteDatabase('RStoreDB');
        StoreContext.clear();
        AjaxUtil.hide()
        location.reload();
    }

    async onClickLogout() {
        let url = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'LOGOUT');
        await AjaxUtil.call(url);
        logout(this.config);
        window.location.hash = '/';
    }

    async confirmEditPwd() {
        if (this.state.pwd.newPassword == this.state.pwd.repeatPassword) {
            let obj = {
                "Oldpwd": this.state.pwd.oldPassword,
                "Newpwd": this.state.pwd.newPassword,
                "Repeatpwd": this.state.pwd.repeatPassword
            };
            let url = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "PWD", "CHANGEPWD");
            let result = await AjaxUtil.call(url, obj, { method: "POST" });
            if (result.Status == "ture") {
                UIMessageHelper.success("success");
                UIDialog.hide("edit-pwd");
            } else {
                this.setState({ pwd: {} });
            }
        } else {
            this.state.pwd.newPassword = "";
            this.state.pwd.repeatPassword = "";
            this.setState({ pwd: this.state.pwd });
            UIMessageHelper.error("Two passwords do not match");
        }
        this.setState({ pwd: {} });

    }
    cancelEdit() {
        this.state.pwd = {};
        this.setState({ pwd: this.state.pwd });
        UIDialog.hide("edit-pwd");
    }

    menuSwitchOn() {
        let menuSwitchOn = $('#menuSwitchOn');
        let menuSwitchOff = $('#menuSwitchOff');
        menuSwitchOn.addClass('icon_display');
        menuSwitchOff.removeClass('icon_display');
        let footCard = $('.foot-card');
        $('.menuNavMenuBar').css({ left: '60px' });
        $('.top-card').css({ width: 'calc(100% - 70px)' });
        if (footCard && footCard.length > 0) {
            footCard.addClass('ExtendFootCard')
        }
        UISideNav.menuSwitch("cloud_sideMenu")
    }
    menuSwitchOff() {
        let menuSwitchOn = $('#menuSwitchOn');
        let menuSwitchOff = $('#menuSwitchOff');
        menuSwitchOff.addClass('icon_display');
        menuSwitchOn.removeClass('icon_display');
        let footCard = $('.foot-card');
        $('.menuNavMenuBar').css({ left: '250px' });
        $('.top-card').css({ width: 'calc(100% - 258px)' });
        if (footCard && footCard.length > 0) {
            footCard.removeClass('ExtendFootCard')
        }
        UISideNav.menuSwitch("cloud_sideMenu")
    }
    SrceenExtend() {
        let SrceenExtend = $('#SrceenExtend');
        let SrceenContract = $('#SrceenContract');
        SrceenExtend.addClass('icon_display');
        SrceenContract.removeClass('icon_display');
        this.fullScreen();
    }
    SrceenContract() {
        let SrceenExtend = $('#SrceenExtend');
        let SrceenContract = $('#SrceenContract');
        SrceenContract.addClass('icon_display');
        SrceenExtend.removeClass('icon_display');
        this.exitFullscreen();
    }
    //全屏
    fullScreen() {
        var element = document.documentElement;
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        }
    }
    //退出全屏 
    exitFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
    openAdmin() {
        window.open(window.location.href);
    }

}