import { UISpinner, If } from 'rainbowui-desktop-core';

export default class index extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isShowChilden : true
    }
  }
  
  render () {
    const goInAdmin = sessionStorage.getItem('goInAdmin')
    return (
      <div class={goInAdmin == 'true' ? "admin-foot-card" : "foot-card"}>
        {/* <div id="footCordLoading" style={{width:'100%', height:'60px',position:'absolute',bottom:'0'}}></div> */}
        {/* <If condition={this.state.isShowChilden}> */}
          {this.props.children} 
        {/* </If> */}
      </div>
        
    )
  }

  // componentDidMount(){
  //   PubSub.subscribe("Tablayout_flag", async (msg, data) => {
  //     if (data) {
  //       UISpinner.hide("footCordLoading");
  //     } else {
  //       UISpinner.show("footCordLoading");
  //     }
  //   })
  // }
}
