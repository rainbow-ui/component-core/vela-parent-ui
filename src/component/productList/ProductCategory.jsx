import { UIText, UISmartPanelGrid, Component, UICell, UIDataTable, UIColumn, UILink, Param, UIBox, UIButton} from "rainbowui-desktop-core";
import { UrlUtil } from 'rainbow-desktop-tools';
import "../../css/productConfig.css";
import { CodeTableService } from 'rainbow-desktop-codetable';
import { UIMltDateTimePicker } from 'rainbowui-desktop-core-mlt';

export default class ProductCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ProductCategroyArr: [],
            showList: false,
            ProductList: []
        }
        this.config = JSON.parse(sessionStorage.getItem('project_config')) || {};
    }
    async getData() {
        let url1 = this.config.UI_API_GATEWAY_PROXY + 'product/prd/v1/productMaster/all';
        let productLineData = await AjaxUtil.call(url1, null, { 'method': 'GET' });
        let productLineList = [];
        let ProductLineCt = await CodeTableService.getCodeTable({ 'CodeTableName': 'ProductLine' });
        let ProductLineCtList = ProductLineCt && ProductLineCt.codes ? ProductLineCt.codes : [];
        productLineData.forEach(itemPrd => {
            let prdLineInfo = {};
            if(itemPrd && itemPrd.ProductLine && itemPrd.ProductTypeId != 1){
                prdLineInfo.Id = itemPrd.ProductLine.ProductLineId;
                prdLineInfo.Description = itemPrd.ProductLine.Description;
                prdLineInfo.Code = itemPrd.ProductLine.Code;
                productLineList.push(prdLineInfo);
            }
        });
        if(productLineList && productLineList.length>0){
            let obj = {};
            productLineList = productLineList.reduce(function(item, next) {
                obj[next.Id] ? '' : obj[next.Id] = true && item.push( next );
                return item;
            }, []);
            if(ProductLineCtList && ProductLineCtList.length > 0){
                productLineList.forEach(line => {
                    ProductLineCtList.forEach(ctItem =>{
                        if(line.Id == ctItem.id){
                            line.Code = ctItem.Code;
                            line.Description = ctItem.text;
                        }
                    })
                })
            }
        }

        this.setState({ProductCategroyArr: productLineList});
        // let url = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'CODETABLE', 'GETCODETABLEBYNAME');
        // let res = await AjaxUtil.call(url, { "CodeTableName":"ProductLine","ConditionMap":{}}, { 'method': 'POST' });
        // console.log(res)
        // if (res && res.BusinessCodeTableValueList && res.BusinessCodeTableValueList.length > 0) {
        //     this.setState({ProductCategroyArr: res.BusinessCodeTableValueList})
        // } else {
        //     this.setState({ProductCategroyArr: []})
        // }
    }

    componentDidMount() {
         this.getData()
    }

    render() {
        const productArray = []
        if (this.state.ProductCategroyArr.length > 0) {
            _.each(this.state.ProductCategroyArr, productItem => {
                productArray.push(
                    <div onClick={this.newProposal.bind(this, productItem.Id, productItem.Code)} className="proposalIconWrap">
                        <UICell className="proposalIconB">
                            {/* <span class={'rainbow ' + productItem.icon}></span> */}
                            <img className="productImg" src={`https://rainbow.ebaotech.com/static/rainbow/image/productline/${productItem.Id}.png`} />
                        </UICell>

                        <div className="itemtext">{productItem.Description}</div>
                    </div>
                )
            })
        }

        const arr = [];
        arr.push(
            <div>
            
            <div class="home-page-button-wrap col-sm-6 col-md-6 col-lg-6">
                <UIBox direction="left">
                    <UIButton id="dynamictopback" styleClass="default" value="Back" onClick={this.goBack.bind(this)} />
                </UIBox>
            </div>
            <UIDataTable id="searchProductDataTable" searchable={true} isOnSearch={true} pageable="true" value={this.state.ProductList} >
            <UIColumn headerTitle="ProductCode" value="ProductCode" width="15%" render={
                            (data) => {
                                return (<UIText io="out" model={data} property="ProductCode" />);
                            }
                        } />
            <UIColumn headerTitle="ProductName" value="ProductName" width="15%" render={
                (data) => {
                    return (<UIText io="out" model={data} property="ProductName" />);
                }
            } />
            {/* <UIColumn headerTitle="templateCode" value="templateCode" width="15%" render={
                (data) => {
                    return (<UIText io="out" model={data} property="ComputerTemplate" />);
                }
            } /> */}
            <UIColumn headerTitle="StartDatetime" value="StartDatetime" width="15%" render={
                (data) => {
                    return (<UIMltDateTimePicker io='out' model={data} property='StartDate' />);
                }
            } />
            <UIColumn headerTitle="EndDatetime" value="EndDatetime" width="15%" render={
                (data) => {
                    return (<UIMltDateTimePicker io='out' model={data} property='EndDate' />);
                }
            } />
            <UIColumn headerTitle="Action" value="Action" width="15%" render={
                (data) => {
                    return (
                    <UILink value="NewProposal" onClick={this.onQueryProduct.bind(this, data.ComputerTemplate, data.ProductCode, data.ProductName)}>
                        <Param name="query" value={data} />
                    </UILink>);
                }
            } />
            </UIDataTable>
            
            </div>
        )
        return (
            !this.state.showList ? 
            <UISmartPanelGrid column='4'>
                {productArray}
            </UISmartPanelGrid>
            :
            <div>
                {arr}
            </div>
        )
    }

    onQueryProduct(templateName, productCode, productName) {
        // sessionStorage.removeItem('productCalculate')
        // sessionStorage.removeItem('tempData')
        // let resultPolicy = {
        //     "ProductCode": data
        // }
        // window.location.hash = '/policyManagement/' + resultPolicy.ProductCode
        // UIDialog.hide("chooseProductDialog");
        if (this.props.callBackEvt) {
            this.props.callBackEvt(templateName, productCode, productName)
        }
    }

    goBack() {
        this.setState({ProductList:[], showList: false});
    }

    async newProposal(id, code) { // Product Data Jump
        let url = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'CODETABLE', 'GETPRODUCTLINEMAP');
        let res = await AjaxUtil.call(url, { "ProductLineId":id, "EffectiveFlag":"Y"}, { 'method': 'POST' });
        if (res && res[code] && res[code].ProductList && res[code].ProductList.length > 0) {
            this.setState({ProductList:res[code].ProductList, showList: true});
        } else {
            this.setState({ProductList:[], showList: true});
        }
        let container2 = $('.modal-body .scrollTable .row.tb-searchbar .col-sm-6');
        let backobj = $('.home-page-button-wrap.col-sm-6');
        backobj.insertBefore(container2);
        $($('.row.tb-searchbar .col-sm-6.col-md-6.col-lg-6')[1]).css({textAlign:'right'});
        $($('#dynamictopback').parent('.boxPadding')[0]).css({paddingTop:'0px'});
    }
}