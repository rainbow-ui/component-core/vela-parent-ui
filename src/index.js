'use strict'

module.exports = {
  UnicornPermissionMenu: require('./component/permissionMenu/PermissionMenu'),
  SideMenu: require('./component/permissionMenu/sideMenu'),
  Filter: require('./component/filter/Filter'),
  CommonConfigBackGround: require('./component/commonconfigbg/CommonConfigBackGround'),
  TopCard: require('./component/topCard/topCard'),
  FootCard: require('./component/footCard/footCard'),
  Login: require('./component/login/login'),
  ProductCategory: require('./component/productList/ProductCategory'),
  TabContainer:require('./component/tab/tabContainer')
}
