module.exports = {
    UserName: "Nome do usuário",
    Password: "Senha",
    Login: "Conecte-se",
    ForgotPassword: "Esqueceu a senha？",
    noAccess: "Você não tem autoridade de acesso. Entre em contato com o administrador do sistema."
};
