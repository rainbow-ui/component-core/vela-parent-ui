module.exports = {
    UserName: "用戶名",
    Password: "密碼",
    Login: "登錄",
    ForgotPassword: "忘記密碼？",
    noAccess: "您沒有訪問權限。 請聯繫您的系統管理員。"
};
