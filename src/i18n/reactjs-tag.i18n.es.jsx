module.exports = {
    UserName: "Nombre de usuario",
    Password: "Contraseña",
    Login: "Iniciar sesión",
    ForgotPassword: "Se te olvidó tu contraseña？",
    noAccess: "No tiene autoridad de acceso. Por favor, póngase en contacto con el administrador del sistema."
};
