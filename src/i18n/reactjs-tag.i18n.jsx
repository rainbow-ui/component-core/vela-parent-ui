import { I18nUtil } from 'rainbowui-desktop-core';

try {
	const projectConfig = JSON.parse(sessionStorage.getItem('project_config'));
	let lang = I18nUtil.getSystemI18N();
	if (projectConfig && projectConfig.telentConfig && projectConfig.telentConfig.defaultLang) {
		lang = projectConfig.telentConfig.defaultLang;
	}
	module.exports = require("./reactjs-tag.i18n." + lang);
} catch(exception) {
	console.error("i18n file read error.");
	module.exports = require("./reactjs-tag.i18n.en_US");
}
