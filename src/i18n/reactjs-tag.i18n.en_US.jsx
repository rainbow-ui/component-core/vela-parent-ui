﻿module.exports = {
    UserName: "User Name",
    Password: "Password",
    Login: "LOGIN",
    ForgotPassword: "Forgot Password？",
    noAccess: "You don’t have access authority. Please contact your system administrator."
};
