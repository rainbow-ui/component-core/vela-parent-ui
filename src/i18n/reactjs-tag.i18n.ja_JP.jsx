﻿module.exports = {
    UserName: "ユーザ名",
    Password: "パスワード",
    Login: "ログイン",
    ForgotPassword: "パスワードをお忘れですか？",
    noAccess: "アクセス権限がありません。 システム管理者に連絡してください。"
};
