﻿module.exports = {
    UserName: "用户名",
    Password: "密码",
    Login: "登录",
    ForgotPassword: "忘记密码？",
    noAccess: "您没有访问权限。 请联系您的系统管理员。"
};
